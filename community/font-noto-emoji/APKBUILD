# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=font-noto-emoji
pkgver=2.040
pkgrel=0
pkgdesc="Google Noto emoji fonts"
url="https://github.com/googlefonts/noto-emoji"
arch="noarch"
license="OFL-1.1"
source="https://github.com/googlefonts/noto-emoji/archive/v$pkgver/font-noto-emoji-$pkgver.tar.gz"
options="!check" # No code to test
builddir="$srcdir/noto-emoji-$pkgver"

# Prior to commit 213931dec8bb08b1d4e500bf06f3892d711e9499 we build
# font-noto-emoji from source. However, some makedependencies were not
# available on all architectures and building everything from source
# blocks some builders for some time, hence using the pre-built now.

package() {
	install -Dm644 -t "$pkgdir"/usr/share/fonts/noto \
		fonts/NotoColorEmoji.ttf
}

sha512sums="
77186620bdc02fdf97b44acb4a542257a234bfc6cc87cde45f850514d992fba80f7a67f385bf865d4395bceed7c7d758241dda7a562b44d4efa5f5cc5568a550  font-noto-emoji-2.040.tar.gz
"
