# Maintainer: kpcyrd <git@rxv.cc>
pkgname=wasm-bindgen
pkgver=0.2.88
pkgrel=0
pkgdesc="Interoperating JS and Rust code"
url="https://github.com/rustwasm/wasm-bindgen"
arch="all"
license="Apache-2.0"
depends="cargo nodejs rust-wasm"
makedepends="
	cargo-auditable
	"
source="https://github.com/rustwasm/wasm-bindgen/archive/refs/tags/$pkgver/wasm-bindgen-$pkgver.tar.gz
	https://gitlab.archlinux.org/archlinux/packaging/packages/wasm-bindgen/-/raw/$pkgver-1/Cargo.lock
	"
options="net !check" # most tests fail outside of x86_64

prepare() {
	default_prepare

	# https://github.com/rustwasm/wasm-bindgen/issues/1819
	mv "$srcdir"/Cargo.lock .

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cd crates/cli
	cargo auditable build --release --frozen
}

check() {
	cd crates/cli
	cargo test --frozen
}

package() {
	install -Dm755 -t "$pkgdir"/usr/bin \
		target/release/wasm-bindgen \
		target/release/wasm-bindgen-test-runner \
		target/release/wasm2es6js
}

sha512sums="
502095ca77d8682a97eb6ba57f64e2fe918b55dd1f79428af1967571a3fe8df8c30590b1bc7025c9959e79638dbc669b30ba116bd8795d445af3ea89d3b3a678  wasm-bindgen-0.2.88.tar.gz
2634819d55311709d71b06ad89e56f1851724ab81df8848d28d84584044f70d114d3386c167346e17a91b1314b327f49bdbecb2ce4e0ac3e95a1e8d8d80e640f  Cargo.lock
"
