# Contributor: Hygna <hygna@proton.me>
# Contributor: Fabricio Silva <hi@fabricio.dev>
# Maintainer: Fabricio Silva <hi@fabricio.dev>
pkgname=pnpm
pkgver=8.10.5
pkgrel=0
pkgdesc="Fast, disk space efficient package manager"
url="https://pnpm.io"
arch="noarch"
license="MIT"
depends="nodejs"
source="https://registry.npmjs.org/pnpm/-/pnpm-$pkgver.tgz"
options="!check" # not implemented
builddir="$srcdir/package"

prepare() {
	default_prepare

	# remove node-gyp
	rm -rf dist/node-gyp-bin dist/node_modules/node-gyp
	# remove windows files
	rm -rf dist/vendor/*.exe

	# remove other unnecessary files
	find . -type f \( \
		-name '.*' -o \
		-name '*.cmd' -o \
		-name '*.bat' -o \
		-name '*.map' -o \
		-name '*.md' -o \
		-name '*.darwin*' -o \
		-name '*.win*' -o \
		-iname 'README*' \) -delete
}

package() {
	local DESTDIR="$pkgdir"/usr/share/node_modules/pnpm

	mkdir -p "$DESTDIR"
	cp -R "$builddir"/* "$DESTDIR"/

	mkdir -p "$pkgdir"/usr/bin
	ln -sf ../share/node_modules/pnpm/bin/pnpm.cjs "$pkgdir"/usr/bin/pnpm
	ln -sf ../share/node_modules/pnpm/bin/pnpx.cjs "$pkgdir"/usr/bin/pnpx
}

sha512sums="
9c161f433d85551c58f1b3a10b18cc3df72b5a02d2caee6566cc05b6f20addef947e795d90c39033bf92de55177ead69d87f62a9daadc91e7a2e3b58f49353a0  pnpm-8.10.5.tgz
"
