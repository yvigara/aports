# Contributor: Jakob Meier <comcloudway@ccw.icu>
# Maintainer: Jakob Meier <comcloudway@ccw.icu>
pkgname=komikku
pkgver=1.28.0
pkgrel=0
pkgdesc="manga reader for GNOME"
url="https://gitlab.com/valos/Komikku"
arch="noarch !s390x" # limited by blueprint-compiler
license="GPL-3.0-only"
depends="
	libadwaita
	py3-beautifulsoup4
	py3-brotli
	py3-colorthief
	py3-dateparser
	py3-emoji
	py3-gobject3
	py3-keyring
	py3-lxml
	py3-magic
	py3-natsort
	py3-piexif
	py3-pillow
	py3-pure_protobuf
	py3-rarfile
	py3-requests
	py3-unidecode
	webkit2gtk-6.0
	"
makedepends="
	blueprint-compiler-dev
	cmake
	desktop-file-utils
	gobject-introspection-dev
	gtk4.0-dev
	libadwaita-dev
	meson
	"
subpackages="$pkgname-lang $pkgname-pyc"
source="https://gitlab.com/valos/Komikku/-/archive/v$pkgver/Komikku-v$pkgver.tar.gz"
builddir="$srcdir/Komikku-v$pkgver"

build() {
	abuild-meson build

	ninja -C build
}

check() {
	meson test -C build --print-errorlog
}

package() {
	DESTDIR="$pkgdir" meson install -C build
}

sha512sums="
ce149b6b123dc6e59e3b5ce400a7cb8a52813feb14fd81edc3502fe07b5d37a774637ad7a834792bf0eefa2f9841f2947d8e7f2dd1294576b06503d3aaeb1bc3  Komikku-v1.28.0.tar.gz
"
