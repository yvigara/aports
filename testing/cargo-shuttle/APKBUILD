# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=cargo-shuttle
pkgver=0.34.0
pkgrel=0
pkgdesc="Cargo command for the Shuttle platform"
url="https://github.com/shuttle-hq/shuttle"
license="Apache"
# s390x, ppc64le, riscv64: blocked by ring crate
arch="all !s390x !ppc64le !riscv64"
makedepends="cargo cargo-auditable"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/shuttle-hq/shuttle/archive/v$pkgver/shuttle-$pkgver.tar.gz"
builddir="$srcdir/shuttle-$pkgver"
# tests require git/submodules to be initialized
options="!check"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo auditable build -p "$pkgname" --release --frozen
	mkdir -p completions/
	local compgen="target/release/$pkgname generate -s"
	$compgen bash >"completions/$pkgname.bash"
	$compgen fish >"completions/$pkgname.fish"
	$compgen zsh >"completions/_$pkgname"
}

package() {
	install -Dm755 "target/release/$pkgname" -t "$pkgdir/usr/bin/"
	install -Dm644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm644 "completions/$pkgname.bash" "$pkgdir/usr/share/bash-completion/completions/$pkgname"
	install -Dm644 "completions/$pkgname.fish" -t "$pkgdir/usr/share/fish/vendor_completions.d"
	install -Dm644 "completions/_$pkgname" -t "$pkgdir/usr/share/zsh/site-functions"
}

sha512sums="
e68bad22bddeb7552beb0ec9328ccf386cc804aa8e7f2c248711ab7f156520caf589c18ce5eed2bd3cfdaef38aca4c42f004b8dd482b2a4a93aadf1e6de1b360  shuttle-0.34.0.tar.gz
"
